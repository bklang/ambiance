defmodule AmbianceFirmware do
  @moduledoc """
  Documentation for AmbianceFirmware.
  """

  @doc """
  Hello world.

  ## Examples

      iex> AmbianceFirmware.hello
      :world

  """
  def hello do
    :world
  end
end
