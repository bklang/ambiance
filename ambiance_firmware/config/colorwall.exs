# In this file we define the attached devices. This configuration
# will determine what services are offered via the API.
use Mix.Config

config :ambiance_api, :entities,
  lights: %{
    "colorwall" => %{
      name: "Colorwall",
      options: %{
        hardware: AmbianceApi.Hardware.APA102,
        device: "spidev0.1",
        led_count: 180,
        max_brightness: 31,
        order: [:b, :g, :r]
      }
    }
  }
