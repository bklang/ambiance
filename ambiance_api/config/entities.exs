# In this file we define the attached devices. This configuration
# will determine what services are offered via the API.
use Mix.Config

config :ambiance_api, :entities,
  lights: %{
    "basic_light" => %{
      name: "Basic Light",
      description: "This is a basic light, probably an LED",
      supported_features: ["effect", "flash", "stop"],
      options: %{
        hardware: AmbianceApi.Hardware.GPIOLight,
        gpio_pins: [26, 13, 23, 24, 27, 6, 5, 12, 22, 17],
      },
    },
    "colorwall" => %{
      name: "Colorwall",
      options: %{
        hardware: AmbianceApi.Hardware.APA102,
        device: "spidev0.0",
        led_count: 180,
        max_brightness: 31,
        order: [:b, :g, :r]
      }
    }
  }
