# This file is responsible for configuring your application
# and its dependencies with the aid of the Mix.Config module.
#
# This configuration file is loaded before any dependency and
# is restricted to this project.

# General application configuration
use Mix.Config

# Configures the endpoint
config :ambiance_api, AmbianceApiWeb.Endpoint,
  url: [host: "localhost"],
  secret_key_base: "3ZHg0kvaoVMZreTTr5mkDJGIxm6Um2IMfH9VdQ7r6kERU5elLoHRxm9mW7bqCeKt",
  render_errors: [view: AmbianceApiWeb.ErrorView, accepts: ~w(json)],
  pubsub: [name: AmbianceApi.PubSub, adapter: Phoenix.PubSub.PG2]

# Configures Elixir's Logger
config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:request_id]

# Use Jason for JSON parsing in Phoenix
config :phoenix, :json_library, Jason
config :phoenix_swagger, json_library: Jason

config :ambiance_api, :phoenix_swagger,
  swagger_files: %{
    "priv/static/swagger.json" => [
      router: AmbianceApiWeb.Router,     # phoenix routes will be converted to swagger paths
      endpoint: AmbianceApiWeb.Endpoint  # (optional) endpoint config used to set host, port and https schemes.
    ]
  }

# Use Circuits modules for interfacing with hardware (usually overridden in dev)
config :ambiance_api, :hardware_interface, :live

# Configure the device entities available on this instance
import_config "entities.exs"

# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
import_config "#{Mix.env()}.exs"
