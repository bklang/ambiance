defmodule AmbianceApi.Hardware.APA102 do
  @moduledoc """
  Driver for APA102 LEDS (aka "DotStar").
  Derived from the Python APA102 library by Martin Erzberger, especially
  these inline docs (Thanks!)
  Appreciation also to Tim from https://cpldcpu.wordpress.com/2014/11/30/understanding-the-apa102-superled/

  Accepted messages:
    - :set_pixel
    - :set_strip
    - :set_max_brightness
    - :show

  Very brief overview of APA102: An APA102 LED is addressed with SPI. The bits
  are shifted in one by one, starting with the least significant bit.
  An LED usually just forwards everything that is sent to its data-in to
  data-out. While doing this, it remembers its own color and keeps glowing
  with that color as long as there is power.
  An LED can be switched to not forward the data, but instead use the data
  to change it's own color. This is done by sending (at least) 32 bits of
  zeroes to data-in. The LED then accepts the next correct 32 bit LED
  frame (with color information) as its new color setting.
  After having received the 32 bit color frame, the LED changes color,
  and then resumes to just copying data-in to data-out.
  The really clever bit is this: While receiving the 32 bit LED frame,
  the LED sends zeroes on its data-out line. Because a color frame is
  32 bits, the LED sends 32 bits of zeroes to the next LED.
  As we have seen above, this means that the next LED is now ready
  to accept a color frame and update its color.
  So that's really the entire protocol:
  - Start by sending 32 bits of zeroes. This prepares LED 1 to update
    its color.
  - Send color information one by one, starting with the color for LED 1,
    then LED 2 etc.
  - Finish off by cycling the clock line a few times to get all data
    to the very last LED on the strip
  The last step is necessary, because each LED delays forwarding the data
  a bit. Imagine ten people in a row. When you yell the last color
  information, i.e. the one for person ten, to the first person in
  the line, then you are not finished yet. Person one has to turn around
  and yell it to person 2, and so on. So it takes ten additional "dummy"
  cycles until person ten knows the color. When you look closer,
  you will see that not even person 9 knows its own color yet. This
  information is still with person 2. Essentially the driver sends additional
  zeroes to LED 1 as long as it takes for the last color frame to make it
  down the line to the last LED.
  """
  require Logger

  use GenServer
  use Bitwise

  alias AmbianceApi.RGBI
  alias AmbianceApi.Entity.Light

  @led_start 0b11100000 # Three "1" bits, followed by 5 brightness bits
  @max_brightness 0b11111 # Brightness is represented in 5 bits
  @interface Module.concat(AmbianceApi.Hardware.interface(), "SPI")
  # TODO: Support bitbanging?

  @doc """
  led_count is the number of LEDs in the string
  max_brightness is 0..31
  order defines the signaling sequence for the LED colors, r,g,b; b,r,g; etc
  """
  def connect(params) do
    {:ok, apa102_pid} = GenServer.start_link(__MODULE__, params)
    %{apa102_pid: apa102_pid}
  end

  @doc """
  Converts %Light state into values for the LED strip.
  """
  def show(%Light{is_on: false, hardware_state: %{apa102_pid: apa102_pid}}) do
    # The light is turned off. Kill all the pixels.
    set_strip(apa102_pid, %RGBI{r: 0, g: 0, b: 0, i: 0})
    GenServer.call(apa102_pid, :show)
  end
  def show(%Light{
    is_on: true,
    brightness: brightness,
    effect: _effect,
    hs_color: [hue, saturation],
    hardware_state: %{apa102_pid: apa102_pid}
  }) do
    # Scale brightness range 0..255 to 0..@max_brightness
    brightness = div brightness * @max_brightness, 255

    # Saturation is expressed as a float with range 0.0-100.0. Tint expects values 0.0-1.0
    rgbi = Tint.HSV.new(hue, saturation/100, 1) |> RGBI.from_tint(brightness)

    # TODO: Effects

    set_strip(apa102_pid, rgbi)
    GenServer.call(apa102_pid, :show)
  end

  def init(%{device: device, led_count: led_count, max_brightness: max_brightness, order: order}) do
    Logger.debug "Starting LED String with #{led_count} LEDs in #{Enum.join(order, ",")} order"

    # Limit the brightness to the maximum if it's set higher
    max_brightness = if max_brightness > @max_brightness do
      Logger.warn "Requested maximum brightness too high (#{inspect max_brightness}), capping at #{@max_brightness}"
      @max_brightness
    else
      max_brightness
    end

    leds = Enum.map(1..led_count, fn(_) -> %RGBI{i: 0} end)

    {:ok, pid} = @interface.open(device)

    {:ok, %{max_brightness: max_brightness, order: order, leds: leds, spi_pid: pid, is_on: false}}
  end

  @doc """
  Sets the color of one pixel in the LED string.
  The changed pixel is not shown yet on the string, it is only
  written to the pixel buffer. Colors are passed individually.
  If brightness is not set the global brightness setting is used.
  """
  def set_pixel(light, index, [r, g, b]) do
    set_pixel(light, index, %RGBI{r: r, g: g, b: b, i: @max_brightness})
  end
  def set_pixel(light, index, [r, g, b, i]) do
    set_pixel(light, index, %RGBI{r: r, g: g, b: b, i: i})
  end
  def set_pixel(light = %Light{hardware_state: %{apa102_pid: apa102_pid}}, index, rgbi = %RGBI{}) do
    :ok = GenServer.call(apa102_pid, {:set_pixel, [index, rgbi]})
    light
  end

  def set_strip(apa102_pid, rgbi = %RGBI{}) do
    # Performance optimization: Rather than mutate a bunch of Maps (one per LED),
    # instead construct a new list of Maps using the same rgbi value
    led_count = get_strip(apa102_pid) |> length()
    leds = Enum.map(1..led_count, fn(_) -> rgbi end)
    set_strip(apa102_pid, leds)
  end
  def set_strip(apa102_pid, leds) do
    GenServer.call(apa102_pid, {:set_strip, [leds]})
  end

  def get_pixel(apa102_pid, index) do
    GenServer.call(apa102_pid, {:get_pixel, [index]})
  end

  def get_strip(apa102_pid) do
    GenServer.call(apa102_pid, {:get_strip})
  end

  def led_count(apa102_pid) do
    GenServer.call(apa102_pid, {:led_count})
  end

  def handle_call({:set_pixel, [index, rgbi = %RGBI{}]}, _from, state = %{leds: leds}) do
    leds = List.replace_at(leds, index, rgbi)
    {:reply, :ok, Map.put(state, :leds, leds)}
  end

  def handle_call({:set_strip, [leds]}, _from, state) do
    {:reply, :ok, Map.put(state, :leds, leds)}
  end

  def handle_call({:set_on_off, new_value}, _from, state) do
    {:reply, :ok, Map.put(state, :is_on, new_value)}
  end

  def handle_call({:get_pixel, [index]}, _from, state = %{leds: leds}) do
    {:reply, Enum.at(leds, index), state}
  end

  def handle_call({:get_strip}, _from, state = %{leds: leds}) do
    {:reply, leds, state}
  end

  def handle_call(:show, _from, state = %{leds: leds, spi_pid: spi_pid, order: order, max_brightness: max_brightness}) do
    clock_start_frame(spi_pid)

    order = [:i] ++ order

    leds
    |> Enum.map(fn(led) ->
      led = Map.put(led, :i, Enum.min([led.i, max_brightness]) ||| @led_start)
      Enum.map(order, fn(key) -> Map.get(led, key) end)
    end)
    |> :binary.list_to_bin()
    |> write(spi_pid)

    leds |> Enum.count() |> clock_end_frame(spi_pid)
    {:reply, :ok, state}
  end

  def handle_call(:get_state, _from, state) do
    {:reply, state, state}
  end

  def handle_call(:led_count, _from, state = %{leds: leds}) do
    {:reply, tuple_size(leds) - 1, state}
  end

  def handle_call(request, from, state) do
    Logger.error "Unhandled Call #{inspect request} - from #{inspect from} - state #{inspect state}"
    {:reply, :ok, state}
  end

  def handle_cast(request, from, state) do
    Logger.error "Unhandled Cast #{inspect request} - from #{inspect from} - state #{inspect state}"
    {:noreply, state}
  end

  def handle_info(request, state) do
    Logger.error "Unhandled Info #{inspect request} - state #{inspect state}"
    {:noreply, state}
  end

  def validate_pixel(rgbi = %RGBI{}) do
    %RGBI{
      r: Enum.max([0, Enum.min([rgbi.r, 255])]),
      g: Enum.max([0, Enum.min([rgbi.g, 255])]),
      b: Enum.max([0, Enum.min([rgbi.b, 255])]),
      i: Enum.max([0, Enum.min([rgbi.i, @max_brightness])])
    }
  end

  @doc """
  Sends a start frame to the LED strip.
  This method clocks out a start frame, telling the receiving LED
  that it must update its own color now.
  """
  def clock_start_frame(spi_pid) do
    write(<<0,0,0,0>>, spi_pid)  # Start frame, 32 zero bits
  end

  @doc """
  Sends an end frame to the LED strip.
  As explained above, dummy data must be sent after the last real colour
  information so that all of the data can reach its destination down the line.
  The delay is not as bad as with the human example above.
  It is only 1/2 bit per LED. This is because the SPI clock line
  needs to be inverted.
  Say a bit is ready on the SPI data line. The sender communicates
  this by toggling the clock line. The bit is read by the LED
  and immediately forwarded to the output data line. When the clock goes
  down again on the input side, the LED will toggle the clock up
  on the output to tell the next LED that the bit is ready.
  After one LED the clock is inverted, and after two LEDs it is in sync
  again, but one cycle behind. Therefore, for every two LEDs, one bit
  of delay gets accumulated. For 300 LEDs, 150 additional bits must be fed to
  the input of LED one so that the data can reach the last LED.
  Ultimately, we need to send additional numLEDs/2 arbitrary data bits,
  in order to trigger numLEDs/2 additional clock changes. This driver
  sends zeroes, which has the benefit of getting LED one partially or
  fully ready for the next update to the strip. An optimized version
  of the driver could omit the clockStartFrame method if enough zeroes have
  been sent as part of clockEndFrame.
  """
  def clock_end_frame(led_count, spi_pid) do
    # Round up num_led/2 bits (or num_led/16 bytes)
    clock_count = led_count + 15 |> div(16)
    Enum.map(0..clock_count, fn(_) -> <<0>> end) |> :binary.list_to_bin() |> write(spi_pid)
  end

  def write(value, spi_pid) do
    @interface.transfer(spi_pid, value)
  end
end
