defmodule AmbianceApi.Hardware.GPIOLight do
  require Logger

  @interface Module.concat(AmbianceApi.Hardware.interface(), "GPIO")
  alias AmbianceApi.Entity.Light

  def connect(%{gpio_pins: gpio_pins}) do
    gpio_pids = Enum.map(gpio_pins, fn(gpio_pin) ->
      {:ok, gpio_pid} = @interface.open(gpio_pin, :output)
      gpio_pid
    end)
    %{gpio_pids: gpio_pids}
  end

  def show(%Light{is_on: is_on, hardware_state: %{gpio_pids: gpio_pids}}) do
    Enum.map(gpio_pids, fn(gpio_pid) ->
      set_gpio(gpio_pid, is_on)
    end)
    :ok
  end

  def set_gpio(gpio_pid, newval) do
    pin = @interface.pin(gpio_pid)
    Logger.debug("Setting pin #{pin} to #{newval}")
    @interface.write(gpio_pid, newval)
  end
end
