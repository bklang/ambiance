defmodule AmbianceApi.Entity.Light do
  require Logger
  @derive {Jason.Encoder, only: [:id, :name, :is_on, :brightness, :effect, :effect_list, :hs_color, :supported_features]}
  @enforce_keys [:id]
  @allowed_for_update [:is_on, :brightness, :effect, :hs_color]
  defstruct [
    :id,
    :name,
    :is_on,
    :brightness,
    :effect,
    :effect_list,
    :hs_color,
    :supported_features,
    :hardware_state,
    :options
  ]

  @default_params %{is_on: false, hs_color: [0.0, 0.0], brightness: 255, effect: "none"}

  alias AmbianceApi.Entity.Light

  def init(params = %{options: %{hardware: hardware}}) do
    params = Map.merge(@default_params, params)
    hardware_state = hardware.connect(params.options)

    params = Map.put(params, :hardware_state, hardware_state)
    light = struct(Light, params)
    hardware.show(light)

    {:ok, light}
  end

  def set(light = %Light{options: %{hardware: hardware}}, params) do
    light = sanitize_updates(light, params)

    :ok = hardware.show(light)

    {:ok, light}
  end

  def sanitize_updates(light, params = %{}) do
    params = for key <- @allowed_for_update, into: %{} do
      value = params[key]
      value = if is_nil(value), do: params[Atom.to_string(key)], else: value
      value = if is_nil(value), do: Map.get(light, key), else: value
      {key, value}
    end

    light
    |> Map.merge(params)
    |> Map.put(:is_on, sanitize_is_on(params.is_on))
    |> Map.put(:hs_color, sanitize_hs_color(params.hs_color))
    |> Map.put(:brightness, sanitize_brightness(params.brightness))
    |> Map.put(:effect, sanitize_effect(params.effect))
  end

  def sanitize_is_on(1), do: true
  def sanitize_is_on(0), do: false
  def sanitize_is_on("on"), do: true
  def sanitize_is_on("off"), do: false
  def sanitize_is_on("true"), do: true
  def sanitize_is_on("false"), do: false
  def sanitize_is_on(value) when is_boolean(value) do
    value
  end
  def sanitize_is_on(_value) do
    Logger.warn("Invalid value requested for is_on; defaulting to false")
    false
  end

  def sanitize_hs_color([hue, saturation]) do
    # Tint seems get upset about 360.0
    # ** (Tint.OutOfRangeError) Value 360.0 is out of range [0,360)
    hue = coerce_float(hue) |> clamp(0.0, 359.9)

    saturation = coerce_float(saturation) |> clamp(0.0, 100.0)

    [hue, saturation]
  end

  def sanitize_brightness(brightness) when is_integer(brightness) do
    clamp(brightness, 0, 255)
  end

  # TODO
  def sanitize_effect(_effect), do: "none"

  def coerce_float(val) when is_float(val) do
    val
  end
  def coerce_float(val) when is_integer(val) do
    val / 1
  end
  def coerce_float(val) when is_binary(val) do
    String.to_float(val)
  end
  def coerce_float(_val) do
    Logger.warn "Illegal float value; defaulting to 0.0"
    0.0
  end

  def clamp(value, min, max) do
    Enum.max([min, Enum.min([max, value])])
  end
end
