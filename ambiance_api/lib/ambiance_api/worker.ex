defmodule AmbianceApi.Worker do

  require Logger

  use GenServer

  alias AmbianceApi.Entity.Light

  def start_link([], opts \\ []) do
    lights = Application.get_env(:ambiance_api, :entities)[:lights]
    opts = Keyword.merge(opts, name: __MODULE__)
    GenServer.start_link(__MODULE__, lights, opts)
  end

  def server do
    Process.whereis(__MODULE__) ||
      raise "could not find process #{__MODULE__}. Have you started the application?"
  end

  def init(lights) do
    Logger.debug "Initializing State #{inspect lights}"
    lights = for {id, light_params} <- lights, into: %{} do
      Logger.debug "Creating Light entity for #{id}"
      {:ok, light} = Map.put(light_params, :id, id) |> Light.init
      {id, light}
    end
    {:ok, %{lights: lights}}
  end

  def handle_call(:get_state, _from, state) do
    {:reply, state, state}
  end
  def handle_call({:update, Light, id, params}, _from, state) do
    light = state[:lights][id]
    {:ok, light} = Light.set(light, params)
    lights = Map.put(state[:lights], id, light)
    {:reply, :ok, Map.put(state, :lights, lights)}
  end
  def handle_call(request, from, state) do
    Logger.error "Received Call #{inspect request} - from #{inspect from} - state #{inspect state}"
    {:reply, :ok, state}
  end

  def handle_cast(request, from, state) do
    Logger.error "Received Cast #{inspect request} - from #{inspect from} - state #{inspect state}"
    {:noreply, state}
  end

  def handle_info(msg, state) do
    Logger.debug "Received Info msg - #{inspect msg} - state #{inspect state}"
    {:noreply, state}
  end

  def get(Light, id) do
    get_state()[:lights][id]
  end

  def update(Light, id, params) do
    GenServer.call(server(), {:update, Light, id, params})
  end

  def get_state do
    GenServer.call(server(), :get_state)
  end
end
