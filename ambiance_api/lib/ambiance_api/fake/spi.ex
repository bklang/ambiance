defmodule AmbianceApi.Fake.SPI do
  require Logger

  use GenServer

  def open(_devname, _spi_opts \\ [], opts \\ []) do
    GenServer.start_link(__MODULE__, [], opts)
  end

  def init(args) do
    {:ok, args}
  end

  def release(_pid) do
    {:noreply}
  end

  def transfer(_pid, data) do
    size = byte_size(data)
    :io_lib.format("SPI[~4..0B]: ~ts", [size, inspect(data, limit: size)])
    |> Logger.debug

    Enum.map(0..byte_size(data), fn(_) -> <<0>> end)
  end
end
