defmodule AmbianceApi.Fake.GPIO do
  use GenServer

  def open(gpio_pin, _direction, _gpio_opts \\ []) do
    GenServer.start_link(__MODULE__, gpio_pin)
  end

  def init(gpio_pin) do
    {:ok, gpio_pin}
  end

  def pin(pid) do
    GenServer.call(pid, {:pin})
  end

  def write(pid, value) do
    GenServer.call(pid, {:write, value})
  end

  def read(pid) do
    GenServer.call(pid, :read)
  end

  def handle_call({:write, _value}, _from, state) do
    {:reply, :ok, state}
  end

  def handle_call({:read}, _from, state) do
    {:reply, state, state}
  end

  def handle_call({:pin}, _from, state) do
    {:reply, state, state}
  end
end
