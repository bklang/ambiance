defmodule AmbianceApi.Hardware do
  def interface(:fake) do
    AmbianceApi.Fake
  end
  def interface(:live) do
    Circuits
  end
  def interface() do
    interface Application.get_env(:ambiance_api, :hardware_interface)
  end
end