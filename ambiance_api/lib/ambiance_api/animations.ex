defmodule AmbianceApi.Animations do
  alias AmbianceApi.Hardware.APA102

  require Logger

  defmacro __using__(_) do
    quote do
      import AmbianceApi.Animations.Helpers

      def run(opts \\ %{}) do
        new_opts = step(opts)
        APA102.show()
        run(new_opts)
      end
    end
  end
end
