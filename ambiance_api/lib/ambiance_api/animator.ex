defmodule AmbianceApi.Animator do
  require Logger

  def start_animating(mode, mode_opts \\ []) do
    Task.start_link(mode, :run, mode_opts)
  end
end
