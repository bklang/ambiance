defmodule AmbianceApiWeb.EntityController do
  use AmbianceApiWeb, :controller
  use PhoenixSwagger

  alias AmbianceApi.Worker
  alias AmbianceApi.Entity.Light

  swagger_path :index do
    get "/entities"
    description "List entities"
    response 200, "Success"
  end

  def index(conn, _params) do
    entities = Worker.get_state

    conn
    |> render("index.json", entities: entities)
  end

  def update(conn, params = %{"id" => id}) do
    Worker.update(Light, id, params)

    conn
    |> send_resp(:no_content, "")
  end

  def swagger_definitions do
    %{
      Light: swagger_schema do
        title "Light"
        description "A device that emits light, possibly colorful and/or animated"
        properties do
          id :string, "unique (to this Ambiance instance) id of this device"
          name :string, "human-friendly name for this device"
          is_on :boolean, "whether the device mix is on or off"
          brightness :number, "how bright the light is, range 0.0-1.0", format: :float
          effect :string, "the name of the currently selected effect"
          effect_list :array, "list of effects this device is able to perform"
          hs_color :array, "current selected color, in hue/saturation format [0.0-1.0]"
          supported_features :array, "list of features supported by this device"
        end
      end,
      Lights: swagger_schema do
        title "Lights"
        description "The collections of lights available on this Ambiance instance"
        type :array
        items Schema.ref(:Light)
      end
    }
  end
end
