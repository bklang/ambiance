defmodule AmbianceApiWeb.StateController do
  use AmbianceApiWeb, :controller

  alias AmbianceApi.Worker
  alias AmbianceApi.Entity.Light

  def set(conn, _params = %{"id" => id, "value" => value})  do
    Worker.get(Light, id)
    |> Light.set(value)

    conn
    |> send_resp(:no_content, "")
  end
end
