defmodule AmbianceApiWeb.Router do
  use AmbianceApiWeb, :router

  pipeline :api do
    plug :accepts, ["json"]
  end

  scope "/api/swagger" do
    forward "/", PhoenixSwagger.Plug.SwaggerUI, otp_app: :ambiance_api, swagger_file: "swagger.json"
  end

  scope "/api", AmbianceApiWeb do
    pipe_through :api

    resources "/entities", EntityController, only: [:index, :show, :update]
  end

  def swagger_info do
    %{
      info: %{
        version: "0.1",
        title: "Ambiance API"
      }
    }
  end
end
