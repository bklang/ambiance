defmodule AmbianceApiWeb.EntityView do
  use AmbianceApiWeb, :view
  def render("index.json", assigns) do
    assigns[:entities]
  end
end

