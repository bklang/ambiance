Ambiance: RPi IoT API
=================================

Ambiance is a project to expose devices from a Raspberry Pi, such as lights, sensors, and switches, via an HTTP/JSON API.

The API makes devices connected to the Raspberry Pi available via a higher-level API. Groups of GPIO pins can be grouped together as a single logical "light", as can LED strips connected via SPI, such as the APA102 family.

The initial motivation of this project was to connect [Home Assistant](https://www.home-assistant.io/) to a collection of home-built LED strips and sensors. However, the API is generic enough to be used for other integrations as well.

This project is based on [Phoenix](https://www.phoenixframework.org/) & [Nerves](https://nerves-project.org/), and is written in [Elixir](https://elixir-lang.org/). Thanks to Nerves, deployment is trivial using SD card images, as well as in-place, over-the-air software updates. The deployments are tested on a Raspberry Pi 3B/3B+, but should work on any platform supported by Nerves.

API documentation can be found by booting up the `ambiance_api` server and navigating to `http://localhost:4000/api/swagger`

### Currently supported devices
* Lights connected and switched via GPIO (no PWM)
* APA102 family LED strips via SPI

### Future Plans
* Genric simple toggle switches via GPIO
* PIR motion sensors via GPIO
* Door/window opening sensors via GPIO


## Features

The Light abstraction allows controls over several capabilities of lights, including:

* On/Off
* Brightness/Dimming
* Colors
* Effects

Over-the-Air updates and easy SD card images are available thanks to Nerves.

## See Also

* [A simple Python client library is available](https://gitlab.com/bklang/ambiance_client_python)
* [A custom Home Assistant component](https://gitlab.com/bklang/ambiance_homeassistant)
